import { Trial, Register } from './lib';
import { keys, getKey, fixationCross, getUserMetadata, httpGet, timeout } from './utils';

function required(argMap) {
    let missing = Object.keys(argMap).filter(k => !argMap[k]);
    if (!missing.length) return;
    let keys = missing.join(' ,');
    throw new Error(`[${keys}] arguments required`);
}

function assert<T>(atype, val: T): T {
    if (Object.getPrototypeOf(atype) !== Object.getPrototypeOf(val)) {
        throw new Error('invalid argument type');
    }
    return val;
}

@Register('instruction')
export class Instruction extends Trial {
    text: string;
    footer: string;
    advanceKeys: string[];
    // TODO: fix advanceKeys
    constructor({text, footer = '', advanceKeys = [' '], ...rest}:
        { text: string, footer: string, advanceKeys: string[] }) {
        super();
        this.text = text;
        this.footer = footer;
        this.advanceKeys = advanceKeys;
        for (let key in rest) {
            this[key] = rest[key];
        }
    }
    runTrial(element: HTMLElement) {
        let eventType = 'keydown';
        return new Promise((resolve, reject) => {
            if (this.footer) {
                this.text += `<br/><br/>${this.footer}`;
            }
            element.innerHTML = `<div class="instructions">${this.text}</div>`;
            let handler = (e) => {
                if (this.advanceKeys.indexOf(getKey(e)) > -1) {
                    window.removeEventListener(eventType, handler);
                    resolve(this);
                }
            };
            window.addEventListener(eventType, handler);
        });
    }
}

export type RSVPArgs = {
    condition: 'joke' | 'nonJoke',
    joke: string,
    nonJoke: string,
    delay?: number,
    responseKeys?: [string, string]
    id: string
};

@Register('rsvp')
export class RSVPTrial implements Trial {
    condition: 'joke' | 'nonJoke';
    delay: number;
    stimuli: string[];
    responseKeys: string[];
    rt: number;
    response: boolean;
    joke: string;
    nonJoke: string;
    id: string;

    constructor({ condition, delay, responseKeys, joke, nonJoke, id, ...rest}: RSVPArgs) {
        this.condition = condition;
        this.stimuli = (condition == 'joke' ? joke : nonJoke)
            .replace(/^\s+|\s+$/g, '')
            .replace(/\s\s+/g, ' ')
            .replace(/([.!?])(?!$)/g, '$1 ')
            .split(/\s/);
        this.delay = delay || 200;
        this.responseKeys = responseKeys || ['z', '/'];
        this.joke = joke;
        this.nonJoke = nonJoke;
        this.id = id;
        for (let key in rest) {
            this[key] = rest[key];
        }
    }

    getFeedback() {
        return new Promise((resolve, reject) => {
            let then = performance.now();
            let handler = (e) => {
                let now = performance.now();
                let key = getKey(e);
                let index = this.responseKeys.indexOf(key);
                if (index == -1) return;
                e.preventDefault();
                window.removeEventListener('keydown', handler);
                this.response = index == 1;
                this.rt = now - then;
                resolve(this);
            };
            window.addEventListener('keydown', handler);
        });
    }

    showStimuli(element) {
        let prevTime = performance.now();
        let index = 0;
        this.stimuli.push('');
        return new Promise((resolve, reject) => {
            let data = [];
            let animationLoop = (time) => {
                var req = window.requestAnimationFrame(animationLoop);
                if (index == this.stimuli.length) {
                    window.cancelAnimationFrame(req);
                    element.innerHTML = '';
                    resolve();
                }
                else if (time - prevTime >= this.delay) {
                    element.innerHTML = `
<div class="rsvp container">
  <div class="content-container">
    <div class="content">
    ${this.stimuli[index++]}
    </div>
  </div>
</div>
`;
                    prevTime = time;
                    data.push(time);
                }
            };
            window.requestAnimationFrame(animationLoop);
        });
    }
    runTrial(element: HTMLElement) {
        return fixationCross(element, 1000)
            .then(() => this.showStimuli(element))
            .then(() => this.getFeedback())
            .catch((e) => console.log(e));
    }
}

@Register('picture-naming')
export class PictureNaming implements Trial {
    imagePath: string;
    response: string;
    onsetTime: number;

    constructor({imagePath}) {
        this.imagePath = imagePath;
    }

    showStimuli(element): Promise<void> {
        element.innerHTML = `
        <div style="text-align: center">
        <div style="height: 300px">
        <img src=${this.imagePath} style= "" />
        </div>
        <div> <input class="response" type= "text" style= "text-align: center"/></div>
        </div>
            `;
        let input = <HTMLElement>document.querySelector('.response');
        input.focus();
        return Promise.resolve();
    }

    getFeedback(element): Promise<PictureNaming> {
        let eventType = 'keydown';
        let node = <HTMLInputElement>document.querySelector('.response');
        return new Promise((resolve, reject) => {
            let handler = (e) => {
                let response = node.value;
                if ((getKey(e) === keys.enter) && !!response.trim()) {
                    window.removeEventListener(eventType, handler);
                    element.innerHTML = '';
                    this.response = response;
                    resolve(this);
                }
            };
            window.addEventListener(eventType, handler);
        });
    }

    runTrial(element: HTMLElement) {
        return this.showStimuli(element)
            .then(() => this.getFeedback(element))
            .catch((e) => console.log(e));
    }
}

type VerbalFluencyGUI = {
    input: HTMLInputElement,
    clock: HTMLInputElement,
    output: HTMLElement
};

@Register('verbal-fluency')
export class VerbalFluency implements Trial {
    category: string;
    response: string[];
    duration: number;
    constructor({category, duration, ...rest}) {
        this.category = category;
        this.response = [];
        this.duration = duration || 60;
        for (let key in rest) {
            this[key] = rest[key];
        }
    }

    private showStimuli(element: HTMLElement): VerbalFluencyGUI {
        element.innerHTML = `
        <div style="text-align: center">
        <div>
        <p>Type as many examples of <big> ${this.category.toUpperCase()} </big> as you can think of in ${this.duration} seconds. </p>
        <p>Press <big> Enter </big> after each example. </p>
        </div>
        <p class="clock"> </p>
        <input class="input" type= "text" style= "text-align: center"> </input>
        <small> <p class="responses"> </small>
        </div>
        </div>
            `;
        let input = <HTMLInputElement>element.querySelector('.input');
        let clock = <HTMLInputElement>element.querySelector('.clock');
        clock.innerHTML = `${this.duration}`;
        input.focus();
        return {
            input: input,
            clock: clock,
            output: <HTMLElement>element.querySelector('.responses')
        };
    }

    private getFeedback(element: HTMLElement, {input, output, clock}: VerbalFluencyGUI): Promise<VerbalFluency> {
        let eventType = 'keydown';
        let handler = (e) => {
            let value = input.value.trim();
            if ((getKey(e) === keys.enter) && !!value) {
                this.response.push(value);
                input.value = '';
                output.innerHTML = this.response.join(', ');
            }
        }
        input.addEventListener('keydown', handler);
        return new Promise((resolve, reject) => {
            let timeRemaining = this.duration;
            let id = window.setInterval(() => {
                timeRemaining--;
                clock.innerHTML = `${timeRemaining}`;
                if (timeRemaining == 0) {
                    window.clearInterval(id);
                    resolve(this);
                }
            }, 1000);
        });
    }

    runTrial(element: HTMLElement): Promise<VerbalFluency> {
        let gui = this.showStimuli(element);
        return this.getFeedback(element, gui);
    }
}

@Register('qualtrics')
export class QualtricsSurvey implements Trial {
    url: string;

    constructor({url}: { url: string }) {
        this.url = assert('', url);
    }

    runTrial(element: HTMLElement): Promise<{type: string, url: string, joinid: string}> {
        let { sessionId } = getUserMetadata();
        element.innerHTML = `
        <div class="qualtrics-container">
        <iframe src="${this.url}?joinid=${sessionId}" height= "100%" width= "100%" > </iframe>
        </div>
            `;
        return new Promise((resolve, reject) => {
            let handler = e => {
                if (e.data == 'closeQSIWindow') {
                    element.innerHTML = '';
                    window.removeEventListener('message', handler);
                    resolve({
                        type: 'qualtrics',
                        url: this.url,
                        joinid: sessionId
                    });
                }
            };
            window.addEventListener('message', handler);
        });
    }
}

function getIpAddress() {
    return Promise.race([
        httpGet('https://icanhazip.com').then(s => (typeof s === 'string') && s.trim()),
        timeout(1000)])
        .catch(() => 0);
}

@Register('capture-metadata')
export class CaptureMetadata implements Trial {
    constructor() {
    }
    async runTrial(element: HTMLElement): Promise<{}> {
        let ip = await getIpAddress();
        return {
            type: 'capture-metadata',
            browserUserAgent: navigator.userAgent,
            browserPlatform: navigator.platform,
            browserLanguage: navigator.language,
            browserIp: ip,
            timestamp: new Date()
        };
    }
}

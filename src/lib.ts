import { UserMetadata, recordData, recordError, getKey } from './utils';

var STORE_DATA = recordData;

export interface timelineObj {
    type: string;
    [key: string]: any;
}

export type timelineArg = timelineObj | Trial;

type trialConstructor = new (...args: any[]) => Trial;

export abstract class Trial {
    abstract runTrial(element: Element): Promise<any>;
}


export interface SelfRecordingTrial {
    runTrial(element: Element): Promise<void>;
}

var registry: { [key: string]: trialConstructor; } = {};

export function registerDataHandler(record: ({data: {}}) => void){
    STORE_DATA = record;
}

// decorator that stores a class in the registry
export function Register(name: string) {
    return function(klass: trialConstructor) {
        registry[name] = klass;
        klass['type'] = name;  // assigned to instance by constructTrial
    }
}

// returns an instance of a class that was stored in the registry
function constructTrial(args: timelineObj) {
    let trial = new registry[args.type](args)
    trial['type'] = args.type;
    return trial;
}

function expandNode(item: timelineObj): timelineObj[] {
    let timeline: timelineObj[] = item['timeline'];
    if (typeof timeline === 'undefined') return [item];
    return timeline
        .map(expandNode)
        .reduce((acc, item) => acc.concat(item))
        .map((nestedItem: timelineObj) => {
            let newItem: timelineObj = { type: item.type };
            for (let key in item) {
                newItem[key] = item[key];
            }
            for (let key in nestedItem) {
                newItem[key] = nestedItem[key];
            }
            delete newItem['timeline'];
            return newItem;
        });
}

// recursively expands nested timeline arrays, returning a flat array.
export function expandTimeline(timeline: timelineArg[]): timelineArg[] {
    return timeline.reduce((acc, item) => {
        if (isTrial(item)) return acc.concat([item]);
        return acc.concat(expandNode(item));
    }, []);
}

function isTrial(x): x is Trial {
    return x.runTrial !== undefined;
}

export function resolveTimeline(timeline: timelineArg[]): Trial[] {
    return timeline.map((args) => {
        if (isTrial(args)) return args;

        let fn = registry[args.type];
        if (!fn) {
            throw Error(`${args.type} is not a valid trial type`);
        }
        return constructTrial(args);
    });
}

var data: Trial[] = [];

export class VOID {
}

export class SKIP {
}

function skipper(key: string | null): Promise<SKIP> {
    return new Promise(resolve => {
        if (!!key) {
            document.addEventListener('keydown', e => {
                if (getKey(e) == key) resolve(SKIP);
            });
        }
    });
}

export function runTimeline(
    root: Element | string,
    timeline: timelineArg[],
    {skipKey = null}: { skipKey: string | null }): Promise<Trial[]> {

    let trials = resolveTimeline(expandTimeline(timeline));
    let index = 0;
    let _root: Element = (typeof root === 'string' ? document.querySelector(root) : root);
    if (!_root) {
        throw new Error(`invalid root arg passed to runTimeline: ${root}`);
    }
    return trials.reduce((acc, trial) => {
        return acc
            .then(() => Promise.race([skipper(skipKey), trial.runTrial(_root)]))
            .then((val) => {
                if (val === VOID) {
                    return;
                }
                if (val === SKIP) {
                    val = { skipped: true };
                }
                data.push(val);
                val['index'] = index++;
                STORE_DATA(val);
            });
    }, Promise.resolve(undefined))
        .catch(e => recordError(e));
}

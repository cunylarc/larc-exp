export const keys = {
    enter: String.fromCodePoint(13)
}

export function getKey(e: KeyboardEvent): string {
    var keynum;

    if (window.event) { // IE
        keynum = e.keyCode;
    } else if (e.which) { // Netscape/Firefox/Opera
        keynum = e.which;
    }

    return String.fromCharCode(keynum).toLowerCase();
}

// Returns a promise that resolves after `number` ms
export function delay(duration: number): Promise<never> {
    let prevTime = performance.now();
    return new Promise((resolve, reject) => {
        let animationLoop = (time: number) => {
            var req = window.requestAnimationFrame(animationLoop);
            if (time - prevTime > duration) {
                window.cancelAnimationFrame(req);
                resolve();
            }
        }
        window.requestAnimationFrame(animationLoop);
    });
}

export function durationalText(element, text, duration) {
    element.innerHTML = text;
    return delay(duration)
        .then(() => { element.innerHTML = ''; });
}

export let fixationCross = (element, duration) => durationalText(
    element,
    `<div class="fixation container">
  <div class="content-container">
    <div class="content">
      +
    </div>
  </div>
</div>`,
    duration);

export function shuffle<T>(a: T[]) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
}

var cached = [];
export function preloadImages(prefix: string, images: string[]) {
    window.setTimeout(() => {
        cached = cached.concat(
            images.map(filename => {
                var image = new Image();
                image.src = `${prefix}/${filename}`;
                return image;
            }))
    }, 1);
}

export function uuid(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


function getStoredJson(key): {} {
    return JSON.parse(localStorage.getItem(key)) || {};
}

function storeJson(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}

function getQueryParams(): {} {
    let qString = window.location.search,
        queryRe = /[?&]([^=]+?)=([^?]+?)(?=&|$)/g,
        triple = null,
        params = {};

    while (triple = queryRe.exec(qString)) {
        let [_, key, val] = triple;
        params[key] = val;
    }

    return params;
}

export interface UserMetadata {
    participantId: string; // randomly assigned on each refresh unless set via
    // query parameter.
    sessionId: string;     // changes on every browser refresh
    browserId: string;     // this persists across experimental runs
}

export function getUserMetadata(): UserMetadata {
    let location = 'metadata',
        queryParams = getQueryParams(),
        storedParams = getStoredJson(location),

        // mturk data
        workerId = queryParams['workerId'] || '',
        assignmentId = queryParams['assignmentId'] || '',
        hitId = queryParams['hitId'] || '',

        metadata = {
            participantId: workerId || queryParams['id'] || uuid(),
            browserId: storedParams['browserId'] || uuid(),
            sessionId: window['sessionId'] || uuid(),
            workerId: workerId,
            assignmentId: assignmentId,
            hitId: hitId
        };

    window['sessionId'] = metadata['sessionId'];

    storeJson(location, metadata);

    return metadata;
}

let METADATA = getUserMetadata();

export function recordData(data: {}) {
    let database = window['firebase'].database().ref(`session/${METADATA.sessionId}`);
    if (!database) {
        throw new Error();
    }
    data['timestamp'] = + new Date();
    data['timestampStr'] = '' + new Date();
    for (let key in METADATA) {
        data[key] = METADATA[key];
    }
    console.log(data);
    database.push(data);
}

export function recordMetadata(data: {}) {
    console.log('adding recordMetadata', data);
    for (let key in data) {
        METADATA['meta_' + key] = data[key];
    }
}

export function recordError(error) {
    var database = window['firebase'].database().ref(
        `errors/participant/${METADATA.sessionId}`);
    database.push({
        participantId: METADATA.participantId,
        sessionId: METADATA.sessionId,
        timestamp: Date.now(),
        error: `${error} `
    });
}

export function httpGet(url) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);

    return new Promise((resolve, reject) => {
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                // Success!
                resolve(request.responseText);
            } else {
                // We reached our target server, but it returned an error
                reject(request);
            }
        };

        request.onerror = function() {
            reject(request);
        };

        request.send();
    });
}

export function timeout(ms: number): Promise<Error> {
    return new Promise((resolve, reject) => {
        setTimeout(() => reject(new Error('Timed out')), ms);
    });
}
